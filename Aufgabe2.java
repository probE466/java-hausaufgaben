package de.hsel.hausaufgabe;

public class Aufgabe2 {
	public static void main(String[] args) {
		double startK = 5000;
		double startK2 = startK;
		double zinsen = 3.8 / 100;
		int jahre = 1;
		double endK = 0;
		while (endK <= startK2 * 2) {
			endK = 0;
			endK = startK * zinsen + startK;
			if (jahre == 10) {
				System.out.println("Das Kapital nach 10 Jahren ist: " + (float) endK);
			} else if (endK >= startK2 * 2) {
				System.out.println("Das Kapital verdoppelt sich nach " + jahre + " Jahren!");
				break;
			}

			startK = endK;
			jahre++;
		}
	}
}
