package de.hsel.hausaufgabe;

public class
Aufgabe1b {
    public static void main(String[] args) {
        double n = 1;
        double n2 = 2;
        double n3 = 3;
        while (n < 8) {
            double erg = n * n2 * n3;
            System.out.println((int) n + "*" + (int) n2 + "*" + (int) n3 + "=" + (int) erg);
            n++;
            n2++;
            n3++;
        }

    }
}
