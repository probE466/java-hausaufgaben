package de.hsel.hausaufgabe;


public class Aufgabe7 {

    private static int value1 = 0;
    private static boolean letzerDurchlauf;

    public static void main(String[] args) {
        int a = 1;
        int b = 1;
        for (int i = 1; i < 51; i++) {
            System.out.print(" " + i);
            if (i < 50) {
                System.out.print(",");
            }
        }
        System.out.println();
        for (int j = 1; j < 100; j++) {
            System.out.print(" " + j);
            if (j < 99) System.out.print(",");
            j++;
        }
        System.out.println();
        for (int k = 0; k < 6; k++) {
            int temp = a;
            a = b;
            b = temp + b;
            System.out.print(" " + a);
            if (k < 5) {
                System.out.print(",");
            }
        }
        System.out.println();
        for (int l = 0; l < 6; l++) {
            if (letzerDurchlauf == false) {
                value1 = value1 + 1;
                System.out.print(" ," + value1);
                letzerDurchlauf = true;
            } else if (letzerDurchlauf == true) {
                value1 = value1 + 3;
                System.out.print(" ," + value1);
                letzerDurchlauf = false;
            }
        }
    }
}
