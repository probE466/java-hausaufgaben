package de.hsel.hausaufgabe;

public class Aufgabe1 {
	public static void main(String[] args) {
		double n = 1;
		while (n < 10) {
			double erg = Math.pow(n, 3);
			System.out.println((int) n + "*" + (int) n + "*" + (int) n + "=" + (int) erg);
			n++;
		}

	}

}
