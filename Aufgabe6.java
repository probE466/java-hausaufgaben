package de.hsel.hausaufgabe;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by larsgrahmann on 05.11.15.
 */
public class Aufgabe6 {

    private static double value1;
    private static double value2;
    private static double flInhalt;
    private static boolean running = true;
    private static String userinput;
    private static Scanner eingabe = new Scanner(System.in);

    public static void main(String[] args) {
        Aufgabe6 aufgabe6 = new Aufgabe6();
        System.out.println("Was soll berechnet werden?");
        System.out.println("--Dreieck, --Kreis, --Quadrat");
        userinput = eingabe.next();
        Choice choice;

        while(running) {
            try {
                choice = aufgabe6.user_in(userinput);
                if (choice == Choice.QUADRAT) {
                    aufgabe6.berechnung(value1, choice.QUADRAT);
                    System.out.println(flInhalt);
                } else if(choice == Choice.KREIS) {
                    aufgabe6.berechnung(value1, choice.KREIS);
                    System.out.println(flInhalt);
                } else if (choice == choice.DREIECK) {

                }
            } catch (InputMismatchException ex) {
                System.out.println("Fehler bei der Eingabe!");
                running = false;
            }
        }
    }

    private Choice user_in (String userinput) throws InputMismatchException {
            switch (userinput) {
                case ("--Quadrat"): {
                    System.out.println("Seitenlänge?");
                    value1 = eingabe.nextInt();
                    return Choice.QUADRAT;

                }
                case ("--Kreis"): {
                    System.out.println("Radius?");
                    value1 = eingabe.nextInt();
            }
                case ("--Dreieck"): {
                    System.out.println("Grundfläche?");
                    value1 = eingabe.nextInt();
                    System.out.println("Höhe?");
                    value2 = eingabe.nextInt();
                }
                default: throw new InputMismatchException();

        }
    }



    private void berechnung(double value1, Choice choice) {
        if( choice == Choice.QUADRAT) flInhalt = value1 * value1;
        if( choice == Choice.KREIS) flInhalt = Math.PI * Math.pow(value1, 2);
        if( choice == Choice.DREIECK) flInhalt = (value1 * value2) / 2;
    }

    private enum Choice {
        KREIS, QUADRAT, DREIECK
    }
}