package de.hsel.hausaufgabe;

/**
 * Created by Lars on 05.11.2015.
 */
public class Aufgabe8 {
    private static int j;
    private static int zeilen = 8;
    private static int spalten = 16;

    public static void main(String[] args) {
        aufgabe1(zeilen, spalten);
        aufgabe2(zeilen, spalten);
        aufgabe3(zeilen, spalten);
        aufgabe4(zeilen, spalten);
        aufgabe5(zeilen, spalten);
        aufgabe6(zeilen, spalten);
    }


    private static void aufgabe1(int zeilen, int spalten) {
        for (int i = 0; i < zeilen; i++) {
            while (j < spalten) {
                System.out.print("+");
                j++;
            }
            System.out.println();
            j = 0;
        }
    }

    private static void aufgabe2(int zeilen, int spalten) {
        boolean letzerDurchlauf = false;
        for (int i = 0; i < zeilen; i++) {
            if (!letzerDurchlauf) {
                System.out.println();
                for (int k = 0; k < spalten; k++) {
                    System.out.print("+");
                    letzerDurchlauf = true;
                }
            } else {
                System.out.println();
                for (int l = 0; l < zeilen; l++) {
                    System.out.print("-");
                    letzerDurchlauf = false;
                    if (l == spalten) {
                        System.out.println();
                    }
                }
            }
        }
    }

    private static void aufgabe3(int zeilen, int spalten) {
        for (int i = 0; i < zeilen / 2; i++) {
            for (int k = 0; k < spalten/2; k++) {
                System.out.print(".");
            }
            for (int k = 0; k < spalten/2; k++) {
                System.out.print("+");
            }
            System.out.println();
        }
        for (int i = 0; i < zeilen / 2; i++) {
            for (int k = 0; k < spalten/2; k++) {
                System.out.print("+");
            }
            for (int k = 0; k < spalten/2; k++) {
                System.out.print(".");
            }
            System.out.println();
        }
    }

    private static void aufgabe4(int zeilen, int spalten) {
        int anzahlPlus = 0;
        for (int i = 0; i < spalten; i++) {
            for (int k = 0; k < (spalten - anzahlPlus); k++) {
                System.out.print(".");
            }
            for (int k = 0; k < anzahlPlus; k++) {
                System.out.print("+");
            }
            anzahlPlus = anzahlPlus + 2;
            if(anzahlPlus == spalten + 2) break; // this is so ghetto.....
            System.out.println();
        }
    }

    private static void aufgabe5(int zeilen, int spalten) {

        int zeile;
        int spalte;

        zeile = 1;
        while (zeile <= zeilen) {
            for (spalte = 1; spalte <= zeilen - zeile; spalte++)
                System.out.print(".");

            for (spalte = 1; spalte <= (2 * zeile - 1); spalte++)
                System.out.print("+");

            for (spalte = 1; spalte <= zeilen - zeile; spalte++)
                System.out.print(".");
            System.out.println();
            zeile++;
        }

    }
    private static void aufgabe6(int zeilen, int spalten)
    {
        int zeile;
        int spalte;
        boolean istO = false;

        for (zeile = 1; zeile <= zeilen; zeile++)
        {
            spalte = 1;
            while (spalte <= zeilen - zeile) {
                System.out.print(".");
                spalte++;
            }
            for (spalte = 1; spalte <= (2 * zeile - 1); spalte++)
                if (zeile > 1 && !istO) {
                    System.out.print("+");
                    istO = true;
                } else {
                    if (zeile > 1 && istO) {
                        System.out.print("o");
                        istO = false;
                    } else System.out.print("+");
                }

            for (spalte = 1; spalte <= zeilen - zeile; spalte++)
            {
                System.out.print(".");
            }
            System.out.println();
        }
    }
}



