package de.hsel.hausaufgabe;

import java.util.Scanner;

/**
 * Created by larsgrahmann on 30.10.15.
 */
public class Aufgabe3 {


	public static void main(String[] args) {
		int alter;
		Scanner scanner = new Scanner(System.in);
		System.out.println("Was ist dein Name?");

		String name = scanner.next();
		System.out.println("Was ist dein Geschlecht, M oder W?");

		String geschlecht = scanner.next();

		System.out.println("Wie alt bist du?");
		alter = scanner.nextInt();

		float wdh = Math.round(alter / 4.0);
		System.out.println(wdh);

		if (alter >= 18) {
			if (geschlecht.equalsIgnoreCase("W")) {
				while ((int) wdh > 0) {
					System.out.println("Guten Tag Frau " + name + "!");
					wdh--;
				}
			}
			else if (geschlecht.equalsIgnoreCase("M")) {
				while ((int)wdh > 0) {
					System.out.println("Guten Tag Herr " + name + "!");
					wdh--;
				}
			}
		}

		if (alter < 18) {
			if (wdh > 3.0) {

				while (wdh > 0) {
					System.out.println("Hallo " + name + "!");
					wdh--;
				}
			} else {
				wdh = 3;
				{
					while (wdh > 0) {
						System.out.println("Hallo " + name + "!");
						wdh--;
					}
				}
			}
		}
	}
}
