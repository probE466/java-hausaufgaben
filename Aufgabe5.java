package de.hsel.hausaufgabe;

import java.util.Scanner;

/**
 * Created by larsgrahmann on 03.11.15.
 */
public class Aufgabe5 {
    private static double randomValue1 = Math.round(Math.random() * 100);
    private static double randomValue2 = Math.round(Math.random() * 100);
    private static double randomValue3 = Math.round(Math.random() * 100);
    private static boolean used1;
    private static boolean used2;
    private static boolean used3;

    public static void main(String[] args) {
        //System.out.println(randomValue1);
        //System.out.println(randomValue2);
        //System.out.println(randomValue3);
        Aufgabe5 aufgabe5 = new Aufgabe5();
        Scanner sc = new Scanner(System.in);
        int succ = 0;
        for (int versuche = 0; versuche <= 8; versuche++) {
            if(succ == 3 && versuche < 3) {
                System.out.println("Einfach toll gemacht. Nahezu genial!");
                break;
            }
            if(succ == 3 && versuche < 6) {
                System.out.println("Gut gemacht Sie sind fast wie Einstein.");
                break;
            }
            if(succ == 3 && versuche < 8) {
                System.out.println("War wohl doch nicht so leicht...");
                break;
            }
            if(succ < 3 && versuche > 8) {
                System.out.println("Üben üben üben, das wird schon!");
                break;
            }
            System.out.println("Rate eine Zahl!");
            int user_input = sc.nextInt();
            while(succ <= 3)
                if(test(user_input)) {
                    System.out.println("Richtig!");
                    succ++;
                    break;
                } else {
                    System.out.println("Versuche es nochmal!");
                    break;
            }
        }
    }

    private static boolean test(int user_input) {
        if (user_input == randomValue1 && !used1) {
            used1 = true;
            return true;
        }
        if (user_input == randomValue2 && !used2) {
            used2 = true;
            return true;
        }
        if (user_input == randomValue3 && !used3) {
            used3 = true;
            return true;
        }
        return false;
    }
}